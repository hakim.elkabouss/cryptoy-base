from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    result = []
    for i in range(n):
        permutation = (a * i + b) % n
        result.append(permutation)
    return result


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    a_inv = pow(a, -1, n)

    result = [(a_inv * (i - b)) % n for i in range(n)]

    return result


def encrypt(msg: str, a: int, b: int) -> str:
    unicodes = str_to_unicodes(msg)
    permutation = compute_permutation(a, b, 0x110000)
    encrypted_unicodes = [permutation[i] for i in unicodes]
    return unicodes_to_str(encrypted_unicodes)


def encrypt_optimized(msg: str, a: int, b: int) -> str:
    unicodes = str_to_unicodes(msg)
    encrypted_unicodes = [(a * i + b) % 0x110000 for i in unicodes]
    return unicodes_to_str(encrypted_unicodes)


def decrypt(msg: str, a: int, b: int) -> str:
    unicodes = str_to_unicodes(msg)
    inverse_permutation = compute_inverse_permutation(a, b, 0x110000)
    decrypted_unicodes = [inverse_permutation[i] for i in unicodes]
    return unicodes_to_str(decrypted_unicodes)


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    unicodes = str_to_unicodes(msg)
    decrypted_unicodes = [(a_inverse * (x - b)) % 0x110000 for x in unicodes]
    return unicodes_to_str(decrypted_unicodes)


def compute_affine_keys(n: int) -> list[int]:
    affine_keys = []
    for i in range(1, n + 1):
        if gcd(i, n) == 1:
            affine_keys.append(i)
    return affine_keys


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    for a_1 in affine_keys:
        if (a * a_1) % n == 1:
            return a_1
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    b = 58

    mod_value = 0x110000

    affine_keys = compute_affine_keys(mod_value)

    for a in affine_keys:
        a_inverse = compute_affine_key_inverse(a, affine_keys, mod_value)

        decrypted_msg = decrypt_optimized(s, a_inverse, b)

        if "bombe" in decrypted_msg:
            return decrypted_msg, (a, b)

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )

    mod_value = 0x110000

    affine_keys = compute_affine_keys(mod_value)

    for a in affine_keys:
        a_inverse = compute_affine_key_inverse(a, affine_keys, mod_value)

        for b in range(mod_value):
            decrypted_msg = decrypt_optimized(s, a_inverse, b)

            if "bombe" in decrypted_msg:
                return decrypted_msg, (a, b)

    raise RuntimeError("Failed to attack")
