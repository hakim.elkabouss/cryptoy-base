from math import (
    gcd,
)

from cryptoy.utils import (
    draw_random_prime,
    int_to_str,
    modular_inverse,
    pow_mod,
    str_to_int,
)


def keygen() -> dict:
    e = 65537
    p = draw_random_prime()
    q = draw_random_prime()
    phi = (p - 1) * (q - 1)

    while gcd(e, phi) != 1:
        p = draw_random_prime()
        q = draw_random_prime()
        phi = (p - 1) * (q - 1)

    d = modular_inverse(e, phi)
    return {"public_key": (e, p * q), "private_key": d}


def encrypt(msg: str, public_key: tuple) -> int:
    msg_int = str_to_int(msg)

    if msg_int >= public_key[1]:
        raise ValueError("Message is too large for the key")

    return pow_mod(msg_int, public_key[0], public_key[1])


def decrypt(msg: int, key: dict) -> str:
    decrypted_int = pow_mod(msg, key["private_key"], key["public_key"][1])
    return int_to_str(decrypted_int)
