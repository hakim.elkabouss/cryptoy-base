import hashlib
import os
from random import (
    Random,
)

import names


def hash_password(password: str) -> str:
    return hashlib.sha3_256(password.encode()).hexdigest()


def random_salt() -> str:
    return bytes.hex(os.urandom(32))


def generate_users_and_password_hashes(
    passwords: list[str], count: int = 32
) -> dict[str, str]:
    rng = Random()  # noqa: S311

    users_and_password_hashes = {
        names.get_full_name(): hash_password(rng.choice(passwords))
        for _i in range(count)
    }
    return users_and_password_hashes


def attack(passwords: list[str], passwords_database: dict[str, str]) -> dict[str, str]:
    users_and_passwords = {}

    hash_to_password = {hash_password(password): password for password in passwords}

    for user, password_hash in passwords_database.items():
        if password_hash in hash_to_password:
            users_and_passwords[user] = hash_to_password[password_hash]

    return users_and_passwords


def fix(
    passwords: list[str], passwords_database: dict[str, str]
) -> dict[str, dict[str, str]]:
    users_and_passwords = attack(passwords, passwords_database)

    new_database = {}

    for user, password in users_and_passwords.items():
        salt = random_salt()
        new_database[user] = {
            "password_hash": hash_password(salt + password),
            "password_salt": salt,
        }

    return new_database


def authenticate(
    user: str, password: str, new_database: dict[str, dict[str, str]]
) -> bool:
    if user in new_database:
        stored_hash = new_database[user]["password_hash"]
        stored_salt = new_database[user]["password_salt"]
        return hash_password(stored_salt + password) == stored_hash

    return False
